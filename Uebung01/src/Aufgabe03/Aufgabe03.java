package Aufgabe03;

public class Aufgabe03 {

    public static void main(String[] args){
        addiereZweiMatrizen();
        multipliziereZweiMatrizen();
    }

    private static void addiereZweiMatrizen() {
        Matrix matrixA = new Matrix(2, 2, 'A');
        Matrix matrixB = new Matrix(2, 2, 'B');

        matrixA.init();
        matrixA.input();
        matrixA.print();

        matrixB.init();
        matrixB.input();
        matrixB.print();

        Matrix ergebnis = matrixA.add(matrixB);
        ergebnis.print();
    }

    private static void multipliziereZweiMatrizen(){
        Matrix matrixA = new Matrix(2,3, 'A');
        Matrix matrixB = new Matrix(3,2, 'B');

        matrixA.init();
        matrixA.input();
        matrixA.print();

        matrixB.init();
        matrixB.input();
        matrixB.print();

        Matrix ergebnis = matrixA.mult(matrixB);
        ergebnis.print();
    }
}
