package Aufgabe03;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Matrix {

    private int spalten;
    private int zeilen;
    private int[][] matrix;
    private char matrixBez;

    public Matrix(int zeilen, int spalten, char matrixBez) {
        this.zeilen = zeilen;
        this.spalten = spalten;
        this.matrixBez = matrixBez;
        matrix = new int[zeilen][spalten];
    }

    //initialisiert die Matrix mit Wert 0
    public void init() {
        IntStream.range(0, zeilen).forEach(zeile -> {
            IntStream.range(0, spalten).forEach(spalte -> {
                matrix[zeile][spalte] = 0;
            });
        });
    }

    //gibt die Matrix am Bildschirm aus
    public void print() {
        System.out.println("Matrix " + Character.toUpperCase(matrixBez));
        IntStream.range(0, zeilen).forEach(zeile -> {
            IntStream.range(0, spalten).forEach(spalte -> {
                System.out.print(matrix[zeile][spalte] + " ");
            });
            System.out.println("\r\n");
        });
    }

    //Liest die Matrix von der Tastatur ein
    public void input() {
        System.out.println("Werte für Matrix " + matrixBez);
        Scanner sc = new Scanner(System.in);
        for (int zeile = 0; zeile < zeilen; zeile++) {
            for (int spalte = 0; spalte < spalten; spalte++) {
                String msg = String.format("Wert für %c(%d, %d): ", Character.toLowerCase(matrixBez), zeile + 1, spalte + 1);
                System.out.print(msg);
                matrix[zeile][spalte] = sc.nextInt();
            }
        }
    }

    //Liefert die Summe aus der Matrix und M
    public Matrix add(Matrix m) {
        if (spalten != m.getSpalten() || m.getZeilen() != zeilen) {
            throw new IllegalArgumentException("Beide Matrizen müssen gleich groß sein!");
        }

        Matrix ergebnisMatrix = new Matrix(zeilen, spalten, 'C');

        for (int zeile = 0; zeile < zeilen; zeile++) {
            for (int spalte = 0; spalte < spalten; spalte++) {
                int ergebnis = matrix[zeile][spalte] + m.getMatrix()[zeile][spalte];
                ergebnisMatrix.setSpezifischenWert(spalte, zeile, ergebnis);
            }
        }
        return ergebnisMatrix;
    }

    //Liefert das Produkt aus der Matrix und M
    public Matrix mult(Matrix m) {
        Matrix ergebnis;

        if (spalten == m.getZeilen()) {
            ergebnis = new Matrix(zeilen, m.getSpalten(), 'C');

            for (int zeile = 0; zeile < zeilen; zeile++) {
                for (int spalte = 0; spalte < m.getSpalten(); spalte++) {
                    int erg = 0;
                    for (int k = 0; k < spalten; k++) {
                        erg += matrix[zeile][k] * m.getMatrix()[k][spalte];
                    }
                    ergebnis.setSpezifischenWert(spalte, zeile, erg);
                }
            }
        } else {
            throw new IllegalArgumentException("Ungültige Matrizenform!");
        }
        return ergebnis;
    }


    //getters

    public int getSpalten() {
        return spalten;
    }

    public int getZeilen() {
        return zeilen;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public char getMatrixBez() {
        return matrixBez;
    }

    //setters
    public void setSpezifischenWert(int zeile, int spalte, int wert) {
        matrix[spalte][zeile] = wert;
    }
}
