package Aufgabe02;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Aufgabe02 {

    public static void main(String[] args) {
        List<Integer> primzahlen = siebDesErathostenes(100000);
        primzahlen.forEach(primzahl -> System.out.print(primzahl + ", "));
    }

    public static List<Integer> siebDesErathostenes(int obereSchranke) {
        int[] tempArray = erzeugeArray(obereSchranke);
        //weiter muss nicht untersucht werden, wenn bis zu diesem Ergebnis die Zahlen betrachtet wurden
        int max = (int) Math.sqrt(obereSchranke) + 2;

        for (int i = 0; i < max; i++) {
            if (tempArray[i] != 0) {
                for (int j = i + 1; j < obereSchranke - 2; j++) {
                    if (tempArray[j] % tempArray[i] == 0) {
                        tempArray[j] = 0;
                    }
                }
            }
        }

        List<Integer> primzahlen = Arrays.stream(tempArray).boxed().collect(Collectors.toList());
        return primzahlen.stream().filter(it -> it != 0).collect(Collectors.toList());
    }

    private static int[] erzeugeArray(int obereSchranke) {
        //0 und 1 werden nicht benötigt, daher obereSchranke-2
        int[] array = new int[obereSchranke - 2];
        IntStream.range(2, obereSchranke).forEach(zahl -> array[zahl - 2] = zahl);
        return array;
    }
}


