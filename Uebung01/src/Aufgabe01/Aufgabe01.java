package Aufgabe01;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class Aufgabe01 {

    private static final Logger logger = Logger.getLogger(Aufgabe01.class.getName());

    public static void main(String[] args) {
        logger.setLevel(Level.FINE);

        IntStream.range(30, 41).forEach(a -> {
            IntStream.range(30, 41).forEach(b -> {
                System.out.println(String.format("a = %d, b = %d", a, b));
                int ergebnisIterativ = euklidischerAlgorithmus(a, b);
                int ergebnisRekursiv = euklidischerAlgorithmusRekursiv(a, b);
                int ergebnisKgV = kleinstesGemeinsamesVielfaches(a, b, ergebnisIterativ);
                System.out.println("Ergebnis iterativ: " + ergebnisIterativ);
                System.out.println("Ergebnis rekurisv: " + ergebnisRekursiv);
                System.out.println("Ergebnis KgV: " + ergebnisKgV);
                System.out.println("Ergebnis Multiplikation: " + a * b);
            });
        });
    }

    public static int euklidischerAlgorithmus(int pA, int pB) {
        int a = pA, b = pB;
        int rest;
        do {
            rest = a % b;
            String msg = String.format("a = %d, b = %d => Rest = %d", a, b, rest);
            logger.log(Level.FINE, msg);
            a = b;
            b = rest;
        } while (rest != 0);

        return a;
    }


    public static int euklidischerAlgorithmusRekursiv(int pA, int pB) {
        int a = pA, b = pB;
        int rest = a % b;
        String msg = String.format("a = %d, b = %d => Rest = %d", a, b, rest);
        logger.log(Level.FINE, msg);
        a = b;
        b = rest;

        if (rest != 0) {
            return euklidischerAlgorithmusRekursiv(a, b);
        } else {
            return a;
        }
    }

    public static int kleinstesGemeinsamesVielfaches(int a, int b, int ggt) {
        return (a * b) / ggt;
    }
}

