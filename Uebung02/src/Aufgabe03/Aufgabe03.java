package Aufgabe03;

import java.util.Stack;
import java.util.stream.IntStream;

public class Aufgabe03 {

    public static void main(String[] args) {
        System.out.println("Rekursive Lösung");
        IntStream.range(0, 6).forEach(m -> {
            IntStream.range(0, 6).forEach(n -> {
                try {
                    String msg = String.format("f(n=%d, m=%d) = %d", n, m, rekursiv(n, m));
                    System.out.println(msg);
                } catch (StackOverflowError e) {
                    System.out.println(String.format("Stack Overflow bei n = %d, m = %d", n, m));
                }
            });
        });
        System.out.println("Iterative Lösung");
        IntStream.range(0, 6).forEach(m -> {
            IntStream.range(0, 6).forEach(n -> {
                try {
                    String msg = String.format("f(n=%d, m=%d) = %d", n, m, iterativ(n, m));
                    System.out.println(msg);
                } catch (StackOverflowError e) {
                    System.out.println(String.format("Stack Overflow bei n = %d, m = %d", n, m));
                }
            });
        });


    }

    private static long rekursiv(long n, long m) {
        if (n == 0) {
            return m + 1;
        }

        return m == 0 ? rekursiv(n - 1, 1) : rekursiv(n - 1, rekursiv(n, m - 1));
    }

    private static long iterativ(long n, long m) {
        Stack<Long> stack = new Stack<>();
        stack.add(m);
        while (!stack.isEmpty()) {
            m = stack.pop();
            if (m == 0 || n == 0) {
                n += m + 1;
            } else {
                stack.add(--m);
                stack.add(++m);
                n--;
            }
        }
        return n;
    }
}
