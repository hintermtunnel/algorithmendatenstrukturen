package Aufgabe01;

public class Aufgabe01 {

    public static void main(String[] args) {
        Registermaschine registermaschine;
        System.out.println("Programm gestartet");
        try {
            registermaschine = new Registermaschine("Uebung02/src/Aufgabe01/befehlsliste.txt");
            registermaschine.executeBefehlsliste();
            System.out.println("Programm erfolgreich beendet");
        } catch (Exception e) {
            System.out.println("Fehler bei der Ausführung der Befehlsliste: " + e.getMessage());
        }
    }
}
