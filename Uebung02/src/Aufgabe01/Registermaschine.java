package Aufgabe01;

import java.io.*;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Registermaschine {
    private File befehlslisteFile;
    private Map<Integer, Befehl> befehle = new HashMap<>();
    //Speicherzelle 00 ist der aktuell geladene Wert
    private Map<Integer, Integer> speicher = new HashMap<>();
    private int programmCounter = 1;

    public Registermaschine(String befehlslistePfad) throws IOException {
        befehlslisteFile = new File(befehlslistePfad);
        befehle = readBefehlsliste();
    }

    public void executeBefehlsliste() {
        if (!befehle.get(befehle.size()).isProgrammende()) {
            throw new InputMismatchException("Das Programm wird nicht korrekt mit HLT terminiert!");
        }

        Befehl naechsterBefehl = befehle.get(programmCounter);
        while (!naechsterBefehl.isProgrammende()) {
            if (naechsterBefehl.isSprungbefehl()) {
                executeSprungbefehl(naechsterBefehl);
            } else {
                executeStandardBefehl(naechsterBefehl);
            }
            programmCounter++;
            naechsterBefehl = befehle.get(programmCounter);
        }
    }

    private Map<Integer, Befehl> readBefehlsliste() throws IOException {
        Map<Integer, Befehl> befehle = new HashMap<>();

        try {
            LineNumberReader lnreader = new LineNumberReader(new FileReader(befehlslisteFile));
            String zeile = lnreader.readLine();
            while (null != zeile) {
                befehle.put(lnreader.getLineNumber(), new Befehl(zeile));
                zeile = lnreader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println(befehlslisteFile.getAbsolutePath());
            throw new IOException(e);
        }

        return befehle;
    }

    private void executeStandardBefehl(Befehl befehl) {
        Integer f0 = speicher.get(0), fAdresseWert = speicher.get(befehl.getAdresse()), fAdresse = befehl.getAdresse();

        switch (befehl.getBefehl()) {
            case "ADD":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                speicher.put(0, f0 + fAdresseWert);
                break;
            case "SUB":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                speicher.put(0, f0 - fAdresseWert);
                break;
            case "MUL":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                speicher.put(0, f0 * fAdresseWert);
                break;
            case "DIV":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                speicher.put(0, executeDivision(f0, fAdresseWert));
                break;
            case "LDA":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                speicher.put(0, fAdresseWert);
                break;
            case "LDK":
                speicher.put(0, fAdresse);
                break;
            case "STA":
                speicher.put(fAdresse, f0);
                break;
            case "INP":
                speicher.put(fAdresse, executeUserInput());
                break;
            case "OUT":
                checkForUninitializedSpeicherzelle(f0, fAdresseWert);
                executeAusgabe(fAdresseWert);
                break;
            default:
                break;
        }
    }

    private void executeSprungbefehl(Befehl befehl) {
        int f0 = speicher.get(0), fAdresse = befehl.getAdresse();
        switch (befehl.getBefehl()) {
            case "JMP":
                programmCounter = fAdresse;
                break;
            case "JEZ":
                programmCounter = f0 == 0 ? fAdresse : programmCounter;
                break;
            case "JNE":
                programmCounter = f0 != 0 ? fAdresse : programmCounter;
                break;
            case "JLZ":
                programmCounter = f0 < 0 ? fAdresse : programmCounter;
                break;
            case "JLE":
                programmCounter = f0 <= 0 ? fAdresse : programmCounter;
                break;
            case "JGZ":
                programmCounter = f0 > 0 ? fAdresse : programmCounter;
                break;
            case "JGE":
                programmCounter = f0 >= 0 ? fAdresse : programmCounter;
                break;
            default:
                break;
        }
    }

    private void executeAusgabe(int fAdresseWert) {
        System.out.println("Ausgabe: " + fAdresseWert);
    }

    private int executeUserInput() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Eingabe: ");
        return sc.nextInt();
    }

    private int executeDivision(int f0, int fAdresse) {
        if (fAdresse == 0) {
            String msg = "Division durch 0, Programm wird abgebrochen. Aktueller PC: " + programmCounter;
            throw new IllegalArgumentException(msg);
        }
        return f0 / fAdresse;
    }

    private void checkForUninitializedSpeicherzelle(Integer f0, Integer fAdresseWert) {
        if (f0 == null || fAdresseWert == null) {
            String msg = "Eine nötige Speicherzelle ist nicht initialisiert! Aktueller PC: " + programmCounter;
            throw new IllegalArgumentException(msg);
        }
    }
}
