package Aufgabe01;

import java.util.InputMismatchException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Befehl {

    private static final List<String> sprungBefehle = List.of("JMP", "JEZ", "JNE", "JLZ", "JLE", "JGZ", "JGE");
    private static final List<String> standardBefehle = List.of("ADD", "SUB", "MUL", "DIV", "LDA", "LDK", "STA", "INP", "OUT", "HLT");
    private static final Pattern befehlPattern = Pattern.compile("([A-Z]{3}) (\\d{1,2})");
    private String befehl;
    private int adresse;
    private boolean isSprungbefehl;
    private boolean isProgrammende;

    public Befehl(String pBefehl) {
        createBefehlFromString(pBefehl);
        validBefehl();
        isProgrammende = befehl.equals("HLT");
    }

    private void createBefehlFromString(String pBefehl) {
        Matcher befehlMatcher = befehlPattern.matcher(pBefehl);
        if (befehlMatcher.find()) {
            befehl = befehlMatcher.group(1);
            adresse = Integer.parseInt(befehlMatcher.group(2));
        } else {
            throw new InputMismatchException(String.format("Befehl %s hat eine ungültige Befehlsform", pBefehl));
        }
    }

    private void validBefehl() {
        boolean isStandardbefehl = standardBefehle.contains(befehl);
        boolean isSprungbefehl = sprungBefehle.contains(befehl);
        if (!isStandardbefehl && !isSprungbefehl) {
            throw new InputMismatchException(String.format("Befehl %s ist kein gültiger Befehl", befehl));
        } else this.isSprungbefehl = isSprungbefehl;
    }

    public String getBefehl() {
        return befehl;
    }

    public int getAdresse() {
        return adresse;
    }

    public boolean isSprungbefehl() {
        return isSprungbefehl;
    }

    public boolean isProgrammende() {
        return isProgrammende;
    }
}
