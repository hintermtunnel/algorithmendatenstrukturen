package Aufgabe01;

import java.util.ArrayList;
import java.util.List;

public class Adjazenzliste {

    public static void main(String[] args) {
        AdjazenzlisteReihe[] adjazenzliste = new AdjazenzlisteReihe[4];
        adjazenzliste[0] = new AdjazenzlisteReihe(0);
        adjazenzliste[0].appendNeueNode(3);
        adjazenzliste[1] = new AdjazenzlisteReihe(1);
        adjazenzliste[1].appendNeueNode(0);
        adjazenzliste[1].appendNeueNode(2);
        adjazenzliste[1].appendNeueNode(3);
        adjazenzliste[2] = new AdjazenzlisteReihe(2);
        adjazenzliste[2].appendNeueNode(1);
        adjazenzliste[3] = new AdjazenzlisteReihe(3);
        adjazenzliste[3].appendNeueNode(0);
        adjazenzliste[3].appendNeueNode(1);
        adjazenzliste[0].printReihe();
        adjazenzliste[1].printReihe();
        adjazenzliste[2].printReihe();
        adjazenzliste[3].printReihe();

        System.out.println("Der Graph ist symmetrisch: " + isSymmetric(adjazenzliste));
    }

    private static boolean isSymmetric(AdjazenzlisteReihe[] adjazenzliste) {
        List<Kante> kanten = new ArrayList<>();

        for (AdjazenzlisteReihe adjazenzlisteReihe : adjazenzliste) {
            int u = adjazenzlisteReihe.startNode.knoten;
            AdjazenzlisteNode cursor = adjazenzlisteReihe.startNode;
            while ((cursor = cursor.nachfolger) != null) {
                kanten.add(new Kante(u, cursor.knoten));
            }
        }

        boolean isSymmetric = false;
        for (Kante e : kanten) {
            for (Kante vergleich : kanten) {
                if (e.u == vergleich.v && vergleich.u == e.v) {
                    isSymmetric = true;
                    break;
                } else {
                    isSymmetric = false;
                }
            }

            if (!isSymmetric) {
                break;
            }

        }

        return isSymmetric;
    }

    private static class AdjazenzlisteNode {
        int knoten;
        AdjazenzlisteNode nachfolger = null;

        AdjazenzlisteNode(int knoten) {
            this.knoten = knoten;
        }

    }

    private static class Kante {
        int u, v;

        Kante(int u, int v) {
            this.u = u;
            this.v = v;
        }

    }

    private static class AdjazenzlisteReihe {
        AdjazenzlisteNode startNode;

        AdjazenzlisteReihe(int startKnoten) {
            startNode = new AdjazenzlisteNode(startKnoten);
        }

        void appendNeueNode(int knoten) {
            AdjazenzlisteNode neueNode = new AdjazenzlisteNode(knoten);
            AdjazenzlisteNode cursorNode = startNode;

            while (cursorNode.nachfolger != null) {
                cursorNode = cursorNode.nachfolger;
            }

            cursorNode.nachfolger = neueNode;
        }

        void printReihe() {
            AdjazenzlisteNode cursorNode = startNode;
            while (cursorNode.nachfolger != null) {
                System.out.print(cursorNode.knoten + "->");
                cursorNode = cursorNode.nachfolger;
            }
            System.out.print("/\r\n");
        }
    }
}
